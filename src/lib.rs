use crate::env_reader::env_var;
use log::*;
use std::path::Path;
use std::time::Duration;

pub mod env_reader;
pub mod sleeper;

pub struct Config {
    pub hosts: String,
    pub paths: String,
    pub global_timeout: u64,
    pub tcp_connection_timeout: u64,
    pub wait_before: u64,
    pub wait_after: u64,
    pub wait_sleep_interval: u64,
}

const LINE_SEPARATOR: &str = "--------------------------------------------------------";

/// Wait for hosts and paths to become available before proceeding.
///
/// # Arguments
///
/// * `sleeper` - A mutable reference to an implementation of the `Sleeper` trait.
/// * `configuration` - A reference to the configuration object.
/// * `timeout_handler` - A mutable closure that will be called if a timeout occurs.
pub fn wait(
    sleeper: &mut dyn crate::sleeper::Sleeper,
    configuration: &Config,
    timeout_handler: &mut dyn FnMut(),
) {
    // Print version information
    info!("{}", LINE_SEPARATOR);
    info!(" wait {}", env!("CARGO_PKG_VERSION"));
    info!("---------------------------");

    // Print configuration details
    debug!("Starting with configuration:");
    debug!(" - Hosts to be waiting for: [{}]", configuration.hosts);
    debug!(" - Paths to be waiting for: [{}]", configuration.paths);
    debug!(
        " - Timeout before failure: {} seconds ",
        configuration.global_timeout
    );
    debug!(
        " - TCP connection timeout before retry: {} seconds ",
        configuration.tcp_connection_timeout
    );
    debug!(
        " - Sleeping time before checking for hosts/paths availability: {} seconds",
        configuration.wait_before
    );
    debug!(
        " - Sleeping time once all hosts/paths are available: {} seconds",
        configuration.wait_after
    );
    debug!(
        " - Sleeping time between retries: {} seconds",
        configuration.wait_sleep_interval
    );
    debug!("{}", LINE_SEPARATOR);

    let wait_before = configuration.wait_before;
    let wait_after = configuration.wait_after;

    if wait_before > 0 {
        info!(
            "Waiting {} seconds before checking for hosts/paths availability",
            wait_before
        );
        info!("{}", LINE_SEPARATOR);
        sleeper.sleep(wait_before);
    }

    sleeper.reset();

    if !configuration.hosts.trim().is_empty() {
        for host in configuration.hosts.trim().split(',') {
            info!("Checking availability of host [{}]", host);
            while !port_check::is_port_reachable_with_timeout(
                &host.trim().to_string(),
                Duration::from_secs(configuration.tcp_connection_timeout),
            ) {
                info!("Host [{}] not yet available...", host);
                if sleeper.elapsed(configuration.global_timeout) {
                    error!(
                        "Timeout! After {} seconds some hosts are still not reachable",
                        configuration.global_timeout
                    );
                    timeout_handler();
                    return;
                }
                sleeper.sleep(configuration.wait_sleep_interval);
            }
            info!("Host [{}] is now available!", host);
            info!("{}", LINE_SEPARATOR);
        }
    }

    if !configuration.paths.trim().is_empty() {
        for path in configuration.paths.trim().split(',') {
            info!("Checking availability of path [{}]", path);
            while !Path::new(path.trim()).exists() {
                info!("Path {} not yet available...", path);
                if sleeper.elapsed(configuration.global_timeout) {
                    error!(
                        "Timeout! After [{}] seconds some paths are still not reachable",
                        configuration.global_timeout
                    );
                    timeout_handler();
                    return;
                }
                sleeper.sleep(configuration.wait_sleep_interval);
            }
            info!("Path [{}] is now available!", path);
            info!("{}", LINE_SEPARATOR);
        }
    }

    if wait_after > 0 {
        info!(
            "Waiting {} seconds after hosts/paths availability",
            wait_after
        );
        info!("{}", LINE_SEPARATOR);
        sleeper.sleep(wait_after);
    }

    info!("wait - Everything's fine, the application can now start!");
    info!("{}", LINE_SEPARATOR);
}

/// Reads environment variables and creates a `Config` struct.
pub fn config_from_env() -> Config {
    let hosts = env_var("WAIT_HOSTS", String::new());
    let paths = env_var("WAIT_PATHS", String::new());
    let global_timeout = to_u64(&env_var("WAIT_TIMEOUT", String::new()), 30);
    let tcp_connection_timeout = to_u64(&env_var("WAIT_HOST_CONNECT_TIMEOUT", String::new()), 5);
    let wait_before = to_u64(&env_var("WAIT_BEFORE", String::new()), 0);
    let wait_after = to_u64(&env_var("WAIT_AFTER", String::new()), 0);
    let wait_sleep_interval = to_u64(&env_var("WAIT_SLEEP_INTERVAL", String::new()), 1);

    // Create a new `Config` struct with the read values
    Config {
        hosts,
        paths,
        global_timeout,
        tcp_connection_timeout,
        wait_before,
        wait_after,
        wait_sleep_interval,
    }
}

/// Converts a string to u64.
///
/// # Arguments
///
/// * `input` - The input string to be converted.
/// * `default` - The default value to be returned if the conversion fails.
///
/// # Returns
///
/// The converted u64 value, or the default value if the conversion fails.
fn to_u64(input: &str, default: u64) -> u64 {
    match input.parse() {
        Ok(parsed_value) => parsed_value,
        Err(_) => default,
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use lazy_static::*;
    use std::env;
    use std::sync::Mutex;

    lazy_static! {
        static ref TEST_MUTEX: Mutex<()> = Mutex::new(());
    }

    #[test]
    fn should_return_uint_value() {
        let value = to_u64("32", 0);
        assert_eq!(32, value)
    }

    #[test]
    fn should_return_zero_when_negative_value() {
        let value = to_u64("-32", 10);
        assert_eq!(10, value)
    }

    #[test]
    fn should_return_zero_when_invalid_value() {
        let value = to_u64("hello", 0);
        assert_eq!(0, value)
    }

    #[test]
    fn should_return_zero_when_empty_value() {
        let value = to_u64("", 11);
        assert_eq!(11, value)
    }

    #[test]
    fn config_should_use_default_values() {
        let _guard = TEST_MUTEX.lock().unwrap();
        set_env("", "", "10o", "10", "", "abc");
        let config = config_from_env();
        assert_eq!("".to_string(), config.hosts);
        assert_eq!(30, config.global_timeout);
        assert_eq!(5, config.tcp_connection_timeout);
        assert_eq!(0, config.wait_before);
        assert_eq!(10, config.wait_after);
    }

    #[test]
    fn should_get_config_values_from_env() {
        let _guard = TEST_MUTEX.lock().unwrap();
        set_env("localhost:1234", "20", "2", "3", "4", "23");
        let config = config_from_env();
        assert_eq!("localhost:1234".to_string(), config.hosts);
        assert_eq!(20, config.global_timeout);
        assert_eq!(23, config.tcp_connection_timeout);
        assert_eq!(2, config.wait_before);
        assert_eq!(3, config.wait_after);
        assert_eq!(4, config.wait_sleep_interval);
    }

    #[test]
    fn should_get_default_config_values() {
        let _guard = TEST_MUTEX.lock().unwrap();
        set_env("localhost:1234", "", "", "", "", "");
        let config = config_from_env();
        assert_eq!("localhost:1234".to_string(), config.hosts);
        assert_eq!(30, config.global_timeout);
        assert_eq!(5, config.tcp_connection_timeout);
        assert_eq!(0, config.wait_before);
        assert_eq!(0, config.wait_after);
        assert_eq!(1, config.wait_sleep_interval);
    }

    fn set_env(
        hosts: &str,
        timeout: &str,
        before: &str,
        after: &str,
        sleep: &str,
        tcp_timeout: &str,
    ) {
        env::set_var("WAIT_BEFORE", before.to_string());
        env::set_var("WAIT_AFTER", after.to_string());
        env::set_var("WAIT_TIMEOUT", timeout.to_string());
        env::set_var("WAIT_HOST_CONNECT_TIMEOUT", tcp_timeout.to_string());
        env::set_var("WAIT_HOSTS", hosts.to_string());
        env::set_var("WAIT_SLEEP_INTERVAL", sleep.to_string());
    }
}
